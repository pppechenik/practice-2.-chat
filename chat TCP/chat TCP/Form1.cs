﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace chat_TCP
{
    public partial class Form1 : Form
    {
    //config
        bool alive = false;
        UdpClient client;
        const int LOCALPORT = 8001; 
        const int REMOTEPORT = 8001; 
        const int TTL = 20;
        const string HOST = "235.5.5.1"; 
        IPAddress groupAddress; 

        string userName;

        public Form1()
        {
            InitializeComponent();
            button1.Enabled = true;
            button2.Enabled = false; 
            button3.Enabled = false;
            textBox2.ReadOnly = true;

            groupAddress = IPAddress.Parse(HOST);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            userName = textBox1.Text;
            textBox1.ReadOnly = true;

            try
            {
                client = new UdpClient(LOCALPORT);
                client.JoinMulticastGroup(groupAddress, TTL);
                Task receiveTask = new Task(ReceiveMessages);
                receiveTask.Start();

                string message = userName + " вошел в чат";
                byte[] data = Encoding.Unicode.GetBytes(message);
                client.Send(data, data.Length, HOST, REMOTEPORT);

                button1.Enabled = false;
                button2.Enabled = true;
                button3.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ReceiveMessages()
        {
            alive = true;
            try
            {
                while (alive)
                {
                    IPEndPoint remoteIp = null;
                    byte[] data = client.Receive(ref remoteIp);
                    string message = Encoding.Unicode.GetString(data);

                    this.Invoke(new MethodInvoker(() =>
                    {
                        string time = Convert.ToString(DateTime.Now);
                        textBox2.Text = time + " " + message + "\r\n" + textBox2.Text;
                    }));
                }
            }
            catch (ObjectDisposedException)
            {
                if (!alive)
                    return;
                throw;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string message = String.Format("{0}: {1}", userName, textBox3.Text);
                byte[] data = Encoding.Unicode.GetBytes(message);
                client.Send(data, data.Length, HOST, REMOTEPORT);
                textBox3.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExitChat();
        }
        private void ExitChat()
        {
            string message = userName + " покидает чат";
            byte[] data = Encoding.Unicode.GetBytes(message);
            client.Send(data, data.Length, HOST, REMOTEPORT);
            client.DropMulticastGroup(groupAddress);

            alive = false;
            client.Close();

            button1.Enabled = true;
            button2.Enabled = false;
            button3.Enabled = false;
        }
    }
}
